Passos para a implementação.
1º crie uma base de dados MySql com o nome hepta, não precisa mais nada, o Hibernate já está configurado para criar o restante.
2º Faça o Download do repositório e no eclipse faça o import utilizando "existing project maven".
3º Como a aplicação usa Thymeleaf e DevTools, não precisa configurar servidor, basta ir no arquivo
que contém o @SpringBootApplication chamado HeptaApplication.java e executar.
Todas as dependencias já estão configuradas.

Aplicação gerada utilizando WebService RESTful SpringBoot com Thymeleaf, Hibernate, MVC, MaterializeCss.