package com.mercadinho.hepta.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.mercadinho.models.Produto;
import com.mercadinho.repository.ProdutoRepository;



@Controller
public class ProdutoController {
	
	@Autowired
	private ProdutoRepository pr;
	

	@RequestMapping(value="/novoProduto", method=RequestMethod.GET)
	public String novo(){
		return "mercado/produto";
	}
	
	@RequestMapping(value="/novoProduto", method=RequestMethod.POST)
	public String novo(Produto produto){
		
		pr.save(produto);		
		
		return "redirect:/novoProduto";
	}
	
	
	@RequestMapping("/produtos")
	public ModelAndView listarProdutos() {
		ModelAndView mv = new ModelAndView("mercado/inicio");
		Iterable<Produto> produto = pr.findAll();
		mv.addObject("produtos", produto);
		return mv;
	}
	
	@RequestMapping("/{codigo}")
	public ModelAndView detalhesProduto(@PathVariable("codigo") long codigo) {
		Produto produto = pr.findByCodigo(codigo);
		ModelAndView mv = new ModelAndView("mercado/detalhesProduto");
		mv.addObject("produtos", produto);
		System.out.println("produtos" + produto);
		return mv;		
	}
	
	@RequestMapping("/deletar")
	public String deletarProduto(long codigo) {
		Produto produto = pr.findByCodigo(codigo);
		pr.delete(produto);
		return "redirect:/produtos";
	}
	
	
	
	@RequestMapping("/editandoProduto")
	public ModelAndView visualizarProduto(long codigo) {
		Produto produto = pr.findByCodigo(codigo);
		ModelAndView mv = new ModelAndView("mercado/editar");
		mv.addObject("produto", produto);
		pr.save(produto);
		return mv;		
	}

	
	
	
	
}
